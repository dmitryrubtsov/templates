Templates
=========
_________________
<!-- TOC -->
* [Templates](#templates)
  * [Requirements:](#requirements-)
  * [Cruft](#cruft)
    * [Creating a New Project](#creating-a-new-project)
    * [Updating a Project](#updating-a-project)
  * [FastAPI](#fastapi)
<!-- TOC -->

## Requirements:
- [Cruft](https://cruft.github.io/cruft/)

## Cruft
### Creating a New Project
```
cruft create --directory FOLDER https://gitlab.pgk.ru/oks/navigator_project/templates.git
```

### Updating a Project
```
cruft update
```


## FastAPI

**authors**  
*exsample*: Your Name <your_email@example.com>, Other Name <other_email@example.com>  
```
Авторы проекта  
```

**project**  
*exsample*: navigator  
```
Название проекта  
```

**app_name**  
*exsample*: FastAPI Template  
```
Название сервиса  
```

**app_slug**  
*exsample*: fastapi-template  
```
Заголовок проекта  
```

**app_name_ru**:  
*exsample*: Шаблон FastAPI
```
Название сервиса (RU)  
```

**description**  
```
Краткое описание сервиса  
```

**repository**  
*exsample*: https://gitlab.pgkweb.ru/oks/navigator_project  

**container_registry**  
*exsample*: nexus.pgk.ru/custom-images  

**service_port**  
*exsample* 8***  
```
Внешний порт сервиса  
```

**oracle_client**   
*exsample*: ''  
```
Oracle Instant Client  
```
