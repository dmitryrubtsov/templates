import asyncio
import os
from collections.abc import AsyncGenerator, Generator

from fastapi import FastAPI

import pytest
from asgi_lifespan import LifespanManager
from httpx import AsyncClient


@pytest.fixture(scope="session")
def event_loop() -> Generator[asyncio.AbstractEventLoop, None, None]:
    try:
        loop = asyncio.get_running_loop()
    except RuntimeError:
        loop = asyncio.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(name="app")
def app_fixture() -> FastAPI:
    """Create a new application for testing"""
    from main import get_application

    app = get_application()
    return app


@pytest.fixture
async def async_client(app: FastAPI) -> AsyncGenerator[AsyncClient, None]:
    """Create test async client"""
    async with LifespanManager(app):
        async with AsyncClient(
            app=app,
            base_url="http://testserver",
            headers={
                "Content-Type": "application/json",
            },
        ) as client:
            yield client
