from alembic.command import downgrade, upgrade
from alembic.script import Script


def test_migrations_stairway(alembic_config, revision: Script) -> None:
    """
    Stairway-тест не требует поддержки и позволяет быстро и дешево находить огромное количество
    распространенных типовых ошибок в миграциях:
    - не реализованные методы downgrade
    - не удаленные типы данных в методах downgrade (например, enum)
    - опечатки и другие ошибки.

    Идея теста заключается в том, чтобы накатывать миграции по одной, последовательно выполняя для
    каждой миграции методы upgrade, downgrade, upgrade.
    """
    upgrade(alembic_config, revision.revision)
    # -1 используется для downgrade первой миграции (т.к. ее down_revision равен None)
    downgrade(alembic_config, revision.down_revision or "-1")
    upgrade(alembic_config, revision.revision)
