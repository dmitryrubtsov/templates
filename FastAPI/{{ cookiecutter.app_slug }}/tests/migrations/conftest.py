import uuid
from pathlib import Path
from types import SimpleNamespace

import pytest
from alembic.script import ScriptDirectory
from sqlalchemy import URL, make_url
from sqlalchemy.ext.asyncio import create_async_engine

from core.settings import get_settings
from migrations.main import make_alembic_config


class TestDataBaseError(Exception):
    """Ошибка тестирования миграций"""


async def create_database(url: str | URL) -> None:
    if isinstance(url, str):
        url = make_url(url)

    dialect_name = url.get_dialect().name
    dialect_driver = url.get_dialect().driver

    if dialect_name == "postgresql" and dialect_driver == "asyncpg":
        create_cmd = "CREATE DATABASE {database} ENCODING 'utf8'"
        grant_cmd = "GRANT ALL PRIVILEGES ON DATABASE {database} TO {username}"
        engine = create_async_engine(url.set(database="postgres"))

        async with engine.connect() as conn:
            connection_fairy = await conn.get_raw_connection()
            if (raw_connection := connection_fairy.driver_connection) is None:
                raise TestDataBaseError("driver_connection is None")
            await raw_connection.execute(create_cmd.format(database=url.database))
            await raw_connection.execute(
                grant_cmd.format(database=url.database, username=url.username)
            )
        await engine.dispose()
    else:
        raise TestDataBaseError(
            f"Driver '{dialect_driver}' and dialect '{dialect_name}' not supported"
        )


async def drop_database(url: str | URL) -> None:
    if isinstance(url, str):
        url = make_url(url)

    dialect_name = url.get_dialect().name
    dialect_driver = url.get_dialect().driver

    if dialect_name == "postgresql" and dialect_driver == "asyncpg":
        engine = create_async_engine(url.set(database="postgres"))

        drop_cmd = "DROP DATABASE {database}"
        terminate_cmd = (
            "SELECT pg_terminate_backend(pg_stat_activity.{pid_column}) FROM pg_stat_activity "
            "WHERE pg_stat_activity.datname = '{database}' AND {pid_column} <> pg_backend_pid()"
        )

        async with engine.connect() as conn:
            version = conn.dialect.server_version_info
            pid_column = "pid" if (version >= (9, 2)) else "procpid"
            connection_fairy = await conn.get_raw_connection()
            if (raw_connection := connection_fairy.driver_connection) is None:
                raise TestDataBaseError("driver_connection is None")
            await raw_connection.execute(
                terminate_cmd.format(pid_column=pid_column, database=url.database)
            )
            await raw_connection.execute(drop_cmd.format(database=url.database))
        await engine.dispose()
    else:
        raise TestDataBaseError(
            f"Driver '{dialect_driver}' and dialect '{dialect_name}' not supported"
        )


def get_revisions():
    options = SimpleNamespace(config="alembic.ini", url=None, name="alembic")
    config = make_alembic_config(options)

    # Получаем директорию с миграциями alembic
    revisions_dir = ScriptDirectory.from_config(config)

    # Получаем миграции и сортируем в порядке от первой до последней
    revisions_files = list(revisions_dir.walk_revisions("base", "heads"))
    revisions_files.reverse()
    parameter_sets = (pytest.param(rev, id=Path(rev.path).stem) for rev in revisions_files)
    return parameter_sets


@pytest.fixture(scope="module", name="db_name")
def db_name_fixture() -> str:
    db_name = "_".join(["pytest", uuid.uuid4().hex])
    return db_name


@pytest.fixture(scope="module", name="db_url")
async def db_url_fixture(db_name) -> URL:
    settings = get_settings()
    db_url = make_url(settings.db.url)
    db_url = db_url.set(database=db_name)

    try:
        await create_database(db_url)
        yield db_url
    finally:
        await drop_database(db_url)


@pytest.fixture(scope="module")
async def async_engine(db_url):
    engine = create_async_engine(db_url)
    try:
        yield engine
    finally:
        await engine.dispose()


@pytest.fixture(scope="module")
def alembic_config(db_url):
    options = SimpleNamespace(
        config="alembic.ini", url=db_url.render_as_string(hide_password=False), name="alembic"
    )
    config = make_alembic_config(options)
    return config


@pytest.fixture(params=get_revisions())
def revision(request):
    return request.param
