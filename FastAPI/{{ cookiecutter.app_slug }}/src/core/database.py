import time
from datetime import timedelta

import structlog
from sqlalchemy import event
from sqlalchemy.engine import Engine
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker
from sqlalchemy.orm import Session, sessionmaker

from core.settings import get_settings


log = structlog.get_logger("sql")

settings = get_settings()


# https://docs.sqlalchemy.org/en/20/faq/performance.html#query-profiling
@event.listens_for(Engine, "before_cursor_execute")
def before_cursor_execute(conn, cursor, statement, parameters, context, executemany):
    conn.info.setdefault("query_start_time", []).append(time.perf_counter())


@event.listens_for(Engine, "after_cursor_execute")
def after_cursor_execute(conn, cursor, statement, parameters, context, executemany):
    query_start_time = conn.info["query_start_time"].pop(-1)
    process_time: timedelta = timedelta(seconds=(time.perf_counter() - query_start_time))
    if not context.isinsert:
        log.info(
            statement.replace("\n", ""),
            sql={
                "url": conn.engine.url,
                "process_time": str(process_time),
                "parameters": parameters,
            },
        )
