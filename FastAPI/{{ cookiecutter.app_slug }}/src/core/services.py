from jose import jwt

from navigator_services.abstract.auth import BearerAuth, ScopeAuth
from navigator_utils.contextvars import current_scope
from navigator_utils.security.users import ServiceUser

from core import get_settings

settings = get_settings()

user = ServiceUser(
    sub=settings.app_slug,
    user_name=settings.app_slug,
)
jwt_token = jwt.encode(
    claims=user.model_dump(by_alias=True),
    key=settings.jwt.secret_key,
    algorithm=settings.jwt.algorithm,
)
jwt_auth = BearerAuth(token=jwt_token)
scope_auth = ScopeAuth(scope=current_scope)
