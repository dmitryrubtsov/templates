from contextlib import asynccontextmanager

from fastapi import FastAPI

from core import get_settings

settings = get_settings()


@asynccontextmanager
async def lifespan(app: FastAPI):
    yield
