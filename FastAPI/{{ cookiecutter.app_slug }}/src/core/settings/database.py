"""Конфигурация БД"""

from typing import Any

from pydantic import BaseModel, Field

from ._types import SqlalchemyStrUrl


class DatabaseSettings(BaseModel):
    """Database Settings"""

    url: SqlalchemyStrUrl = SqlalchemyStrUrl("sqlite://")
    echo: bool = False
    pool_size: int = 5
    max_overflow: int = 5
    pool_timeout: int = 30
    pool_recycle: int = -1
    pool_pre_ping: bool = False
    connect_args: dict[str, Any] = Field(default_factory=dict)

    model_config = {"extra": "allow"}
