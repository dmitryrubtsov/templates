"""Настройки сервисов"""

from pydantic import BaseModel, Field

from ._types import HttpStrUrl


class ServicesSettings(BaseModel):
    """Services Settings"""

    disloc_api: HttpStrUrl = Field(
        "http://disloc-api",
        description="Сервис дислокации",
    )
    duration_api: HttpStrUrl = Field(
        "http://duration-api",
        description="Сервис расчета продолжительности операций",
    )
    invoices_api: HttpStrUrl = Field(
        "http://invoices-service",
        description="Сервис накладных",
    )
    kafka_adapter_api: HttpStrUrl = Field(
        "http://kafka-adapter",
        description="Прокси-агент для kafka-consumers",
    )
    mail_service: HttpStrUrl = Field(
        "http://mail-service",
        description="Сервис рассылок",
    )
    nsi_api: HttpStrUrl = Field(
        "http://nsi",
        description="Сервис НСИ для навигатора",
    )
    park_balance_api: HttpStrUrl = Field(
        "http://park-balance-api",
        description="Сервис Баланс парка",
    )
    plan_fact_fwo_api: HttpStrUrl = Field(
        "http://plan-fact-fwo-api",
        description="Сервис План-Факт-ВЗ",
    )
    prognoz_prib_api: HttpStrUrl = Field(
        "http://prognoz-prib",
        description="Сервис Прогноз прибытия",
    )
    provision_api: HttpStrUrl = Field(
        "http://provision",
        description="Сервис обеспечения",
    )
    recommendations_api: HttpStrUrl = Field(
        "http://recommendations-api",
        description="API Рекомендаций",
    )
    reference_info_api: HttpStrUrl = Field(
        "http://reference-info",
        description="Сервис справочной информации для Навигатора",
    )
    standards_api: HttpStrUrl = Field(
        "http://standards-api",
        description="Сервис нормативов",
    )
    telegram_api: HttpStrUrl = Field(
        "http://telegram",
        description="Сервис рассылки сообщений в телеграм"
    )
    wagons_arrival_api: HttpStrUrl = Field(
        "http://wagons-arrival",
        description="Сервис расчета и хранения данных по прибытию вагонов на станцию",
    )

    ssl_verify: bool = True
