"""Конфигурация Uvicorn"""

import typing

from pydantic import BaseModel, Field


class UvicornSettings(BaseModel):
    """Uvicorn Settings"""

    host: str = Field("0.0.0.0", description="Bind socket to this host.")
    port: int = Field(8000, description="Bind socket to this port.")
    reload: bool = Field(None, description="Enable auto-reload.")
    workers: int | None = Field(None, description="Use multiple worker processes")
    access_log: bool = False
    factory: bool = True
    log_config: dict[str, typing.Any] = {
        "version": 1,
        "disable_existing_loggers": False,
    }
    model_config = {"extra": "allow"}
