"""Конфигурация процесса миграций"""

from pydantic import BaseModel, Field


class MigrationsSettings(BaseModel):
    """Background Settings"""

    max_attempt_number: int = Field(3, alias="try")
    wait: int = Field(10, alias="try_delay")
