"""Конфигурация Sentry"""

import typing
from enum import auto

from pydantic import AnyHttpUrl, BaseModel, field_validator
from navigator_utils.types import StrEnum

class Environment(StrEnum):
    """Окружения"""
    PROD = auto()
    PREVIEW = auto()
    TEST = auto()
    DEV = auto()


class SentrySettings(BaseModel):
    """Sentry Settings"""

    dsn: AnyHttpUrl | None = None
    env: Environment | None = None
    traces_sample_rate: float = 1.0

    @field_validator("env", mode="before")
    @classmethod
    def _enum_upper(cls, v: typing.Any) -> typing.Any:
        if isinstance(v, str):
            v = v.upper()
        return v
