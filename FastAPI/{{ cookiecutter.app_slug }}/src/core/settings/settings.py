"""Модуль общих настроек приложения"""

import typing
from enum import IntEnum, auto
from functools import cache
from pathlib import Path

from pgk_logger.settings import LogSettings
from pydantic import Field, ValidationInfo, field_validator
from pydantic_settings import BaseSettings, SettingsConfigDict

from __version__ import __version__
from .brotli import BrotliSettings
from .cors import CorsSettings
from .database import DatabaseSettings
from .jwt import JwtSettings
from .kafka import KafkaSettings
from .migrations import MigrationsSettings
from .redis import RedisSettings
from .s3 import S3Settings
from .sentry import SentrySettings
from .services import ServicesSettings
from .uvicorn import UvicornSettings


class RunMode(IntEnum):
    api = auto()
    background = auto()


class Settings(BaseSettings):
    """{{ cookiecutter.app_name }} settings."""

    project_name: str = "{% if cookiecutter.app_name_ru %}{{ cookiecutter.app_name_ru }}{% else %}{{ cookiecutter.app_name }}{% endif %}"
    app_slug: str = "{{ cookiecutter.app_slug }}"
    api_prefix: str = ""
    root_path: str = ""
    openapi_url: str = "/openapi.json"
    release: str = Field(None, alias="sentry_release")

    debug: bool = False
    # параметр для выполнения миграций, установить False для локального запуска
    migrate: bool = True
    run_mode: RunMode = RunMode.api

    aws: S3Settings = Field(default_factory=dict)
    cors: CorsSettings = Field(default_factory=dict)
    brotli: BrotliSettings = Field(default_factory=dict)
    db: DatabaseSettings = Field(default_factory=dict)
    jwt: JwtSettings = Field(default_factory=dict)
    kafka: KafkaSettings = Field(default_factory=dict)
    log: LogSettings = Field(default_factory=dict)
    migrations: MigrationsSettings = Field(default_factory=dict)
    redis: RedisSettings = Field(default_factory=dict)
    sentry: SentrySettings = Field(default_factory=dict)
    services: ServicesSettings = Field(default_factory=dict)
    uvicorn: UvicornSettings = Field(default_factory=dict)

    model_config = SettingsConfigDict(
        env_nested_delimiter="__",
        secrets_dir="/run/secrets" if Path("/run/secrets").exists() else None,
    )

    @field_validator("run_mode", mode="before")
    @classmethod
    def _get_run_mode(cls, run_mode: RunMode | str) -> RunMode:
        if isinstance(run_mode, str):
            try:
                return RunMode[run_mode.lower()]
            except KeyError as error:
                raise ValueError(
                    f"Unexpected value {run_mode}, must be one of "
                    f"{', '.join(RunMode._member_names_)}"
                ) from error
        return run_mode

    @field_validator("release", mode="before")
    @classmethod
    def _set_release(cls, value: typing.Any, info: ValidationInfo) -> str:
        if value is None:
            app_slug = info.data.get("app_slug")
            value = f"{app_slug}@{__version__}"
        return value

    @field_validator("log", mode="before")
    @classmethod
    def _log_validator(
        cls, log_settings: dict[str, typing.Any], info: ValidationInfo
    ) -> dict[str, typing.Any]:
        if logger := info.data.get("app_slug"):
            log_settings["logger"] = logger
        if info.data.get("debug") is True:
            log_settings["log_level"] = "DEBUG"
        return log_settings

@cache
def get_settings() -> Settings:
    """Получение и кэширование настроек проекта."""
    settings_ = Settings()

    return settings_


settings = get_settings()
