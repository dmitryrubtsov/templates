"""Конфигурация Redis"""

from pydantic import BaseModel, SecretStr


class RedisSettings(BaseModel):
    """Redis Settings"""

    host: str = "localhost"
    port: int = 6379
    username: str | None = None
    password: SecretStr = SecretStr("")
    db: int | None = 0
    namespace: str = "NAVIGATOR"
