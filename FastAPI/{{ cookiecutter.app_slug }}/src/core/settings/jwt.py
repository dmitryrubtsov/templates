"""Конфигурация JWT"""

from pydantic import BaseModel


class JwtSettings(BaseModel):
    """JSON Web Token Settings"""

    algorithm: str = "HS512"
    secret_key: str = ""
