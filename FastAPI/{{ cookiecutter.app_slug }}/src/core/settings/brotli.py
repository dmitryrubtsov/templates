"""Конфигурация сжатия"""

from typing import Literal

from pydantic import BaseModel, Field


class BrotliSettings(BaseModel):
    """Cross-origin resource sharing Settings"""

    quality: int = Field(
        4,
        ge=0,
        le=11,
        description=(
            "Controls the compression-speed vs compression density tradeoff. "
            "The higher the quality, the slower the compression."
        ),
    )
    lgwin: int = Field(22, ge=10, le=24, description="Base 2 logarithm of the sliding window size")
    mode: Literal["generic", "text", "font"] = "text"
    minimum_size: int = 400
    gzip_fallback: bool = True
