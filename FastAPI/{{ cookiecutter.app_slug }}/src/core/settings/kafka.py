"""Конфигурация Kafka"""

from datetime import timedelta
from tempfile import TemporaryDirectory
from typing import Literal

from pydantic import BaseModel, field_validator

from ._types import KafkaStrUrl


class KafkaSettings(BaseModel):
    """Kafka Settings"""

    dsn: KafkaStrUrl | list[KafkaStrUrl] | None = None
    namespace: str = "NAVIGATOR"

    datadir: str = TemporaryDirectory().name
    web_host: str = "localhost"
    web_port: int = 6066
    # https://faust-streaming.github.io/faust/userguide/settings.html#web-enabled
    web_enabled: bool = False
    # https://faust-streaming.github.io/faust/userguide/settings.html#web-in-thread
    web_in_thread: bool = False
    # https://faust-streaming.github.io/faust/userguide/settings.html#topic-disable-leader
    topic_disable_leader: bool = True
    # https://faust-streaming.github.io/faust/userguide/settings.html#broker-max-poll-records
    broker_max_poll_records: int | None = None
    # https://faust-streaming.github.io/faust/userguide/settings.html#broker-commit-interval
    broker_commit_interval: float | timedelta = 2.8
    # https://faust-streaming.github.io/faust/userguide/settings.html#broker-commit-every
    broker_commit_every: int = 10_000
    # https://faust-streaming.github.io/faust/userguide/settings.html#broker-max-poll-interval
    broker_max_poll_interval: float = 1_000.0
    # https://faust-streaming.github.io/faust/userguide/settings.html#broker-heartbeat-interval
    broker_heartbeat_interval: int = 3
    # https://faust-streaming.github.io/faust/userguide/settings.html#consumer-auto-offset-reset
    consumer_auto_offset_reset: str = "latest"
    # https://faust-streaming.github.io/faust/userguide/settings.html#producer-compression-type
    producer_compression_type: Literal["gzip", "snappy", "lz4", "zstd"] | None = None
    # https://faust-streaming.github.io/faust/userguide/settings.html#producer-max-request-size
    producer_max_request_size: int = 1024 * 1024
    # https://faust-streaming.github.io/faust/userguide/settings.html#stream-wait-empty
    stream_wait_empty: bool = True

    @field_validator("dsn", mode="before")
    @classmethod
    def dsn_array(cls, v):
        v = v.split(",")
        if len(v) == 1:
            v = v[0]
        return v
