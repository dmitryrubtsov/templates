from typing import Annotated, Any

from pydantic import GetCoreSchemaHandler, UrlConstraints
from pydantic_core import CoreSchema, core_schema


# TODO Сделать валидацию
class UrlStr(str):
    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))


HttpStrUrl = Annotated[UrlStr, UrlConstraints(max_length=2083, allowed_schemes=["http", "https"])]
SqlalchemyStrUrl = Annotated[UrlStr, UrlConstraints()]
KafkaStrUrl = Annotated[UrlStr, UrlConstraints()]
AWSStrUrl = Annotated[UrlStr, UrlConstraints()]
