"""Конфигурация CORS"""

from pydantic import BaseModel, field_validator


class CorsSettings(BaseModel):
    """Cross-origin resource sharing Settings"""

    allow_origins: list[str] = ["*"]
    allow_credentials: bool = False
    allow_headers: list[str] = ["*"]
    allow_methods: list[str] = ["*"]
    expose_headers: list[str] = ["X-Request-ID"]
    max_age: int = 600

    @field_validator(
        "allow_origins", "allow_headers", "allow_methods", "expose_headers", mode="before"
    )
    @classmethod
    def _assemble_list(cls, v: str | list[str]) -> list[str]:
        if isinstance(v, str):
            v = [i.strip() for i in v.strip("[]").split(",")]
        return v
