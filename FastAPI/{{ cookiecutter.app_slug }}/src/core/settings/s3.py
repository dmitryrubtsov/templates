"""Конфигурация S3"""

import typing

from pydantic import AnyUrl, BaseModel, Field, SecretStr

from ._types import AWSStrUrl


class S3Settings(BaseModel):
    """S3 Settings"""

    url: AWSStrUrl | None = None
    schema_: typing.Literal["http", "https"] = Field("https", alias="schema")
    verify_ssl: bool = True
    access_key_id: str | None = None
    secret_access_key: SecretStr = SecretStr("")
    bucket: str | None = None
    timeout: int = 30
