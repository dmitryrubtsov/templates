#!/usr/bin/env python

import sentry_sdk
from app import main as run_api
from structlog import get_logger

from pgk_logger import setup_logging

from background import main as run_background
from core import RunMode, get_settings
from migrations import run_migrations

log = get_logger()


def main() -> None:
    settings = get_settings()

    INTEGRATIONS = []

    setup_logging(**settings.log.model_dump())
    sentry_sdk.init(
        dsn=settings.sentry.dsn,
        environment=settings.sentry.env,
        release=settings.release,
        traces_sample_rate=settings.sentry.traces_sample_rate,
        integrations=INTEGRATIONS,
    )

    run_migrations(db_url=settings.db.url, migrate=settings.migrate)

    match settings.run_mode:
        case RunMode.api:
            run_api()
        case RunMode.background:
            run_background()


if __name__ == "__main__":
    main()
