from collections.abc import Callable
from typing import Any, Awaitable

from structlog import get_logger

HealthcheckReturn = dict[str, Any] | bool
ConditionFunc = Callable[..., HealthcheckReturn | Awaitable[HealthcheckReturn]]

log = get_logger("api")


CONDITIONS: list[ConditionFunc] = []
