from enum import Enum
from typing import Any
from pydantic import RootModel
from fastapi import APIRouter

from fastapi_health import health

from .conditions import CONDITIONS

router = APIRouter()


class Status(str, Enum):
    success = "available"
    failure = "failure"


async def handler(**kwargs) -> dict[str, Any]:
    output = {}
    for name, value in kwargs.items():
        match value:
            case bool():
                output[name] = Status.success if value else Status.failure
            case _:
                output[name] = value
    return output


router.add_api_route(
    "/healthcheck",
    health(CONDITIONS, success_handler=handler, failure_handler=handler),
    name="debug:healthcheck",
    response_model=RootModel[dict[str, str]],
)
