from typing import Annotated, TypedDict

from fastapi import Security

from sentry_sdk.hub import Hub

from navigator_utils.security.http import JWTBearer, User

from core import get_settings


class SentryUser(TypedDict):
    id: int
    username: str


settings = get_settings()


get_current_user = Security(
    JWTBearer(jwt_secret_key=settings.jwt.secret_key, jwt_algorithm=settings.jwt.algorithm),
    use_cache=True,
)

UserDep = Annotated[User, get_current_user]


async def current_user_sentry(current_user: UserDep):
    """
    Extracts user information and adds it to Sentry's scope.
    """
    hub = Hub.current
    with hub.configure_scope() as sentry_scope:
        user_info: SentryUser = {
            "id": current_user.user_id,
            "username": current_user.user_name,
        }

        sentry_scope.user = user_info

        return current_user
