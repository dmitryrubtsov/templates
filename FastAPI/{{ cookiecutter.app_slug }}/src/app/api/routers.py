from fastapi import APIRouter, Depends

from core.settings import get_settings

from .depends import current_user_sentry
from .healthcheck import router as router_healthcheck

settings = get_settings()

private = APIRouter(dependencies=[Depends(current_user_sentry)])

router = APIRouter()
router.include_router(private)

router.include_router(router_healthcheck, prefix="", tags=["Healthcheck"])
