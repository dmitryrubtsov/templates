from fastapi import FastAPI
from fastapi.responses import ORJSONResponse
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware

from app.api import router
from structlog import get_logger

from navigator_utils.contextvars import current_scope
from navigator_utils.exception_handlers import add_exception_handlers
from navigator_utils.middleware import (
    BrotliMiddleware,
    ConnectionManagerMiddleware,
    CorrelationIdMiddleware,
)
from pgk_logger.middleware import (
    AccessLogMiddleware,
    CurrentScopeSetMiddleware,
    StructlogMiddleware,
)

from __version__ import __version__
from core import get_settings
from core.lifespan import lifespan

log = get_logger()


def get_app() -> FastAPI:
    """Get FastAPI app"""
    settings = get_settings()

    app_ = FastAPI(
        title=settings.project_name,
        version=__version__,
        root_path=settings.root_path,
        default_response_class=ORJSONResponse,
        openapi_url=settings.openapi_url,
        lifespan=lifespan,
        middleware=[
            Middleware(CurrentScopeSetMiddleware, scope_context_var=current_scope),
            Middleware(CorrelationIdMiddleware),
            Middleware(StructlogMiddleware),
            Middleware(AccessLogMiddleware),
            Middleware(BrotliMiddleware, **settings.brotli.model_dump()),
            Middleware(ConnectionManagerMiddleware),
            Middleware(CORSMiddleware, **settings.cors.model_dump(exclude_none=True)),
        ],
    )

    app_.include_router(router, prefix=settings.api_prefix)

    add_exception_handlers(app_)
    return app_


def main() -> None:
    import uvicorn

    settings = get_settings()
    uvicorn.run(get_app, **settings.uvicorn.model_dump())


if __name__ == "__main__":
    main()
