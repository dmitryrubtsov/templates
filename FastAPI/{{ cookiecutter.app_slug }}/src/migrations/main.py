from argparse import Namespace
from pathlib import Path
from typing import Any

from alembic import command
from alembic.config import Config
from sqlalchemy.engine.url import URL

PROJECT_PATH = Path(__file__).parent.parent.parent.resolve()


class MigrationsError(Exception):
    """Ошибка миграции"""


class SimpleNamespace(Namespace):
    def __init__(self, *, config: str | Path, url: str, name: str, **kwargs: Any) -> None:
        super().__init__(config=config, url=url, name=name, **kwargs)
        self.config = config
        self.url = url
        self.name = name


def make_alembic_config(cmd_opts: SimpleNamespace) -> Config:
    if not Path(cmd_opts.config).is_absolute():
        cmd_opts.config = PROJECT_PATH / cmd_opts.config

    config = Config(file_=cmd_opts.config, ini_section=cmd_opts.name, cmd_opts=cmd_opts)

    # Подменяем путь до папки с alembic на абсолютный
    alembic_location = config.get_main_option("script_location")
    if not alembic_location:
        raise MigrationsError("Параметр alembic_location не задан")
    if not Path(alembic_location).is_absolute():
        config.set_main_option("script_location", str(PROJECT_PATH / alembic_location))
    if cmd_opts.url:
        config.set_main_option("sqlalchemy.url", cmd_opts.url)

    return config


def main(db_url: str | URL | None = None, migrate: bool = False) -> None:
    if db_url and migrate:
        db_url_str = (
            db_url.render_as_string(hide_password=False) if isinstance(db_url, URL) else str(db_url)
        )
        options = SimpleNamespace(config="alembic.ini", url=db_url_str, name="alembic")
        alembic_cfg = make_alembic_config(options)

        command.upgrade(alembic_cfg, "head")
