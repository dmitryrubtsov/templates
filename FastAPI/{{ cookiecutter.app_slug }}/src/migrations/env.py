import asyncio
import logging
import sys

from alembic import context
from sqlalchemy import Connection, Engine, engine_from_config, pool, text
from sqlalchemy.ext.asyncio import AsyncEngine
from structlog import get_logger
from tenacity import after_log, retry, stop_after_attempt, wait_fixed

from core.settings import get_settings
from core.settings.database import DatabaseSettings

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = None

settings = get_settings()

log = get_logger("migration")


def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


@retry(
    stop=stop_after_attempt(settings.migrations.max_attempt_number),
    wait=wait_fixed(settings.migrations.wait),
    reraise=True,
    after=after_log(log, logging.WARN),
)
def do_run_migrations(connection: Connection) -> None:
    context.configure(connection=connection, target_metadata=target_metadata)
    with context.begin_transaction():
        for schema in {table.schema or "public" for table in target_metadata.tables.values()}:
            connection.execute(text(f"CREATE SCHEMA IF NOT EXISTS {schema}"))

        context.run_migrations()


async def run_async_migrations_online(connectable_: Engine) -> None:
    """Run migrations in 'async online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    async with AsyncEngine(connectable_).connect() as connection:
        await connection.run_sync(do_run_migrations)


config_section = config.get_section(config.config_ini_section, {})
if config_section.get("sqlalchemy.url") is None:
    log.warning("Database url for migration is not set.")
    db_url = None
    for field, model in settings.model_fields.items():
        if model.annotation is DatabaseSettings:
            db_settings: DatabaseSettings = getattr(settings, field)
            db_url = db_settings.url
            break

    if db_url is None:
        log.error("The database is not configured")
        sys.exit(0)

    config_section["sqlalchemy.url"] = db_url

log.info(f"Apply migration for {config_section['sqlalchemy.url']}")


def run_migrations_online(connectable_: Engine) -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    with connectable_.connect() as connection:
        do_run_migrations(connection)


if context.is_offline_mode():
    run_migrations_offline()
else:
    connectable = engine_from_config(
        config_section,
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
        future=True,
    )
    if connectable.dialect.is_async:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(run_async_migrations_online(connectable))
    else:
        run_migrations_online(connectable)
