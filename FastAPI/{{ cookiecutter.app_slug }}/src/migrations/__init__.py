from .main import main as run_migrations

__all__ = ("run_migrations",)
