from structlog import get_logger

from core import get_settings

log = get_logger("background")
settings = get_settings()


def main() -> None:
    raise NotImplementedError

if __name__ == "__main__":
    main()
