from functools import cache


@cache
def get_version() -> str:
    version = "0.0.0"
    from pathlib import Path

    pyproject = Path(__file__).parent.parent.joinpath("pyproject.toml")
    if pyproject.is_file():
        try:
            import tomllib

            data = tomllib.loads(pyproject.read_text())
            version = data.get("tool", {}).get("poetry", {}).get("version")
        except Exception:
            pass

    del Path
    return version


__version__ = get_version()
