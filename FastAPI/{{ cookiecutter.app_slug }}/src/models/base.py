"""Модуль базовых классов для SQLAlchemy моделей"""

from pydantic.alias_generators import to_snake
from sqlalchemy.orm import DeclarativeBase, MappedAsDataclass, declared_attr


class Base(MappedAsDataclass, DeclarativeBase, kw_only=True):
    """Базовый класс SQLAlchemy модели"""

    @declared_attr.directive
    @classmethod
    def __tablename__(cls) -> str:
        return to_snake(cls.__name__)
