[tool.poetry]
name = "{{ cookiecutter.app_name }}"
version = "0.0.1"
description = "{{ cookiecutter.description }}"
authors = [ {% for author in cookiecutter.authors.split(',') %}"{{ author.strip() }}"{% if not loop.last %}, {% endif %}{% endfor %} ]
readme = "README.md"

[tool.poetry.dependencies]
python = "^3.11,<3.13"
alembic = "^1"
asgi-correlation-id = "^4"
fastapi = "^0.110"
asgi-lifespan = "^2"
fastapi-health = "^0.4"
h11 = "^0.14"
orjson = "^3.9.10"
pydantic = "^2.7"
pydantic-core = "^2"
annotated-types = "^0.6"
pydantic-settings = "^2"
python-dotenv = "^1"
sentry-sdk = {extras = ["asyncpg", "fastapi", "sqlalchemy", "starlette"], version = "^1.31.0"}
sqlalchemy = "^2.0.21"
structlog = ">=24"
structlog-sentry = "^2.0.3"
pgk-logger = ">=0.2"
tenacity = "^8.2.2"
uvicorn = "^0.25.0"
httptools = "^0.6.1"
uvloop = {version = ">=0.18", platform = "linux"}
brotli_asgi = "^1.4"
navigator-services = {extras = ["sentry"], version = ">=1.0"}
navigator-utils = {extras = ["api", "security"], version = ">=1.0"}

[tool.poetry.group.dev.dependencies]
cruft = "^2.15.0"
mypy = "^1.6.1"
sqlalchemy = {extras = ["mypy"], version = ">=2"}

[tool.poetry.group.types.dependencies]
types-python-jose = "^3"
types-redis = "^4"

[tool.poetry.group.linter.dependencies]
ruff = "^0.1"

[tool.poetry.group.test.dependencies]
httpx = ">=0.23"
pytest = "^7.3.2"
pytest-asyncio = "^0.21.0"
pytest-cov = "^5.0"
pytest-mock = "^3.12.0"

[tool.poetry.group.docs.dependencies]
mkdocs-material = "^9.1.19"
mkdocstrings = {extras = ["python"], version = "^0.22.0"}

[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"

[[tool.poetry.source]]
name = "pgk"
url = "https://nexus.pgk.ru/repository/pypi-group/simple/"
priority = "default"

[tool.pytest.ini_options]
addopts = "--import-mode=importlib"
asyncio_mode = "auto"
testpaths = [
    "tests",
]

[tool.ruff]
line-length = 100
target-version = "py311"
select = [
    "E",  # pycodestyle errors
    "W",  # pycodestyle warnings
    "F",  # pyflakes
    "I",  # isort
    "C",  # flake8-comprehensions
    "B",  # flake8-bugbear
    "UP",  # pyupgrade
]
ignore = [
    "E501",  # line too long, handled by black
    "B008",  # do not perform function calls in argument defaults
    "C901",  # too complex
    "W191", # indentation contains tabs
]

[tool.ruff.lint.isort]
known-first-party = [
    "api",
    "background",
    "core",
    "migrations",
    "models",
    "__version__"
]
section-order = [
    "future",
    "standard-library",
    "fastapi",
    "third-party",
    "pkg_libs",
    "first-party",
    "local-folder"
]

[tool.ruff.lint.isort.sections]
"fastapi" = ["fastapi", "starlette"]
"pkg_libs" = ["pgk_logger", "navigator_utils", "navigator_services"]

[tool.ruff.format]
# Like Black, use double quotes for strings.
quote-style = "double"

# Like Black, indent with spaces, rather than tabs.
indent-style = "space"

# Like Black, respect magic trailing commas.
skip-magic-trailing-comma = false

# Like Black, automatically detect the appropriate line ending.
line-ending = "auto"

[tool.mypy]
files = "src/*"

[[tool.mypy.overrides]]
module = "navigator_services"
ignore_missing_imports = true

[[tool.mypy.overrides]]
module = "navigator_utils"
ignore_missing_imports = true
