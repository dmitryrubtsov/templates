#!/bin/sh

while [ -e ".poetry_run" ]; do
    echo "Waiting for a poetry install running in another container (.poetry_run)"
    sleep 1
done

if [ -f "pyproject.toml" ] && [ ! -e ".venv/" ]; then
    touch .poetry_run
    poetry install --no-interaction
    rm -f .poetry_run
fi

exec "$@"