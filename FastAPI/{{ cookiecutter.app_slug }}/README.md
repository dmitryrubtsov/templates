{% if cookiecutter.app_name_ru %}{{ cookiecutter.app_name_ru }}{% else %}{{ cookiecutter.app_name }}{% endif %}
=========
_________________

[![Main pipeline status]({{ cookiecutter.repository }}/{{ cookiecutter.app_slug }}/badges/main/pipeline.svg?key_text=Main+pipeline&key_width=150)]({{ cookiecutter.repository }}/{{ cookiecutter.app_slug }}/commits/main)
[![Main coverage report]({{ cookiecutter.repository }}/{{ cookiecutter.app_slug }}/badges/main/coverage.svg?key_text=Main+coverage&key_width=150)]({{ cookiecutter.repository }}/{{ cookiecutter.app_slug }}/commits/main)  
[![Pre-Production pipeline status]({{ cookiecutter.repository }}/{{ cookiecutter.app_slug }}/badges/preview/pipeline.svg?key_text=Pre-Production+pipeline&key_width=150)]({{ cookiecutter.repository }}/{{ cookiecutter.app_slug }}/commits/preview)
[![Pre-Production coverage report]({{ cookiecutter.repository }}/{{ cookiecutter.app_slug }}/badges/preview/coverage.svg?key_text=Pre-Production+coverage&key_width=150)]({{ cookiecutter.repository }}/{{ cookiecutter.app_slug }}/commits/preview)  
[![Test pipeline status]({{ cookiecutter.repository }}/{{ cookiecutter.app_slug }}/badges/test/pipeline.svg?key_text=Test+pipeline&key_width=150)]({{ cookiecutter.repository }}/{{ cookiecutter.app_slug }}/commits/test)
[![Test coverage report]({{ cookiecutter.repository }}/{{ cookiecutter.app_slug }}/badges/test/coverage.svg?key_text=Test+coverage&key_width=150)]({{ cookiecutter.repository }}/{{ cookiecutter.app_slug }}/commits/test)  

<!-- TOC -->
* [{% if cookiecutter.app_name_ru %}{{ cookiecutter.app_name_ru }}{% else %}{{ cookiecutter.app_name }}{% endif %}](#{% if cookiecutter.app_name_ru %}{{ cookiecutter.app_name_ru }}{% else %}{{ cookiecutter.app_name }}{% endif %})
  * [Servers](#servers)
    * [Production](#production)
    * [Pre-Production](#pre-production)
    * [Development](#development)
    * [Test](#test)
  * [Requirements](#requirements)
<!-- TOC -->
{% if cookiecutter.description %}{{ cookiecutter.description }}{% endif %}

## Servers
http://{{ cookiecutter.app_slug }}

### Production
https://digital.dpprod.pgk.ru/api/{{ cookiecutter.app_slug }}  
[Argo CD](https://argocd.pgk.ru/applications/{{ cookiecutter.app_slug }}-prod)  
[Vault](https://vault-kube.dpprod.pgk.ru/ui/vault/secrets/navi/show/{{ cookiecutter.app_slug }}/prod/config)  
[Log](https://kibana.dpprod.pgk.ru/app/discover#/?_g=(refreshInterval:(pause:!f,value:30000),time:(from:now-1d,to:now))&_a=(columns:!(message),filters:!((query:(match_phrase:(kubernetes.namespace_name:navi))),(query:(match_phrase:(kubernetes.labels.app:navigator))),(query:(match_phrase:(kubernetes.labels.component:'{{ cookiecutter.app_slug }}')))),hideChart:!t,index:'1602b610-1da1-11ed-81d6-97233d9b50d8',interval:auto,sort:!(!('@timestamp',desc))))  
[Swagger documentation](http://digital.dpprod.pgk.ru/api/{{ cookiecutter.app_slug }}/docs)  
[ReDoc documentation](http://digital.dpprod.pgk.ru/api/{{ cookiecutter.app_slug }}/redoc)  
[OpenAPI Specification](http://digital.dpprod.pgk.ru/api/{{ cookiecutter.app_slug }}/openapi.json)

### Pre-Production
https://digital-preview.dp.pgk.ru/api/{{ cookiecutter.app_slug }}  
[Argo CD](https://argocd-dev.pgk.ru/applications/{{ cookiecutter.app_slug }}-preprod)  
[Vault](https://vault-kube-dev.dp.pgk.ru/ui/vault/secrets/navi/show/{{ cookiecutter.app_slug }}/preview/config)  
[Log](https://kibana-dev.dp.pgk.ru/app/discover#/?_g=(refreshInterval:(pause:!f,value:30000),time:(from:now-1d,to:now))&_a=(columns:!(message),filters:!((query:(match_phrase:(kubernetes.namespace_name:navi-preprod))),(query:(match_phrase:(kubernetes.labels.app:navigator))),(query:(match_phrase:(kubernetes.labels.component:'{{ cookiecutter.app_slug }}')))),hideChart:!t,index:'9b038c90-e59d-11ec-a2ea-2b2ec4d08e07',interval:auto,sort:!(!('@timestamp',desc))))  
[Swagger documentation](https://digital-preview.dp.pgk.ru/api/{{ cookiecutter.app_slug }}/docs)  
[ReDoc documentation](https://digital-preview.dp.pgk.ru/api/{{ cookiecutter.app_slug }}/redoc)  
[OpenAPI Specification](https://digital-preview.dp.pgk.ru/api/{{ cookiecutter.app_slug }}/openapi.json)

### Test
https://digital-test.dp.pgk.ru/api/{{ cookiecutter.app_slug }}  
[Argo CD](https://argocd-dev.pgk.ru/applications/{{ cookiecutter.app_slug }}-test)  
[Vault](https://vault-kube-dev.dp.pgk.ru/ui/vault/secrets/navi/show/{{ cookiecutter.app_slug }}/test/config)  
[Log](https://kibana-dev.dp.pgk.ru/app/discover#/?_g=(refreshInterval:(pause:!f,value:30000),time:(from:now-1d,to:now))&_a=(columns:!(message),filters:!((query:(match_phrase:(kubernetes.namespace_name:navi-test))),(query:(match_phrase:(kubernetes.labels.app:navigator))),(query:(match_phrase:(kubernetes.labels.component:plan-fact-fwo-api)))),hideChart:!t,index:'9b038c90-e59d-11ec-a2ea-2b2ec4d08e07',interval:auto,sort:!(!('@timestamp',desc))))  
[Swagger documentation](https://digital-test.dp.pgk.ru/api/{{ cookiecutter.app_slug }}/docs)  
[ReDoc documentation](https://digital-test.dp.pgk.ru/api/{{ cookiecutter.app_slug }}/redoc)  
[OpenAPI Specification](https://digital-test.dp.pgk.ru/api/{{ cookiecutter.app_slug }}/openapi.json)

## Requirements
- [Debian](www.debian.org/) / [Ubuntu](ubuntu.com/) / [Windows Subsystem for Linux](docs.microsoft.com/en-us/windows/wsl/install-win10)
- [Docker and Docker-compose](www.docker.com/)
- [make](www.gnu.org/software/make/)
- [Poetry](python-poetry.org/)
- [Cruft](cruft.github.io/cruft/)
